Installation
--------------
#STEP1: clone this repository
#STEP2: make sure you have got PHP7 and a RDMBS running on your machine.
#STEP3: Navigate to the folder you cloned the project and run `composer install`
#STEP4: When prompted for parameters, make sure you enter the correct values. 
The following scripts will use these parameters to set up the database tables for you. 
#STEP5: run `bin/console server:run`
#STEP6: Open `http://localhost:8000` in your browser.
