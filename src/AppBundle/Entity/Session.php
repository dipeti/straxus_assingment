<?php
/**
 * Created by PhpStorm.
 * User: dipet
 * Date: 2017. 09. 28.
 * Time: 22:55
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`sessions`")
 */
class Session
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $sess_id;

    /**
     * @ORM\Column(type="blob")
     */
    private $sess_data ;

    /**
     * @ORM\Column(type="integer")
     */

    private $sess_time;

    /**
     * @ORM\Column(type="integer")
     */
    private $sess_lifetime;
}