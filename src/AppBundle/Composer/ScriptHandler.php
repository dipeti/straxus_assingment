<?php

namespace AppBundle\Composer;

use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler as BaseScriptHandler;
use Composer\Script\Event;

class ScriptHandler extends BaseScriptHandler
{
    /**
     * Call the app:install:assets command of the AppBundle.
     *
     * @param $event Event instance
     */
    public static function installAssets(Event $event)
    {
        $options = static::getOptions($event);
        $consoleDir = static::getConsoleDir($event, 'app install assets');

        if (null === $consoleDir)
        {
            return;
        }

        $webDir = $options['symfony-web-dir'];

        $symlink = '';
        if ('symlink' == $options['symfony-assets-install'])
        {
            $symlink = '--symlink ';
        }
        elseif ('relative' == $options['symfony-assets-install'])
        {
            $symlink = '--symlink --relative ';
        }

        static::executeCommand($event, $consoleDir, 'app:assets:install '.$symlink.escapeshellarg($webDir), $options['process-timeout']);
    }

    public static function initDb(Event $event)
    {
        $consoleDir = static::getConsoleDir($event, 'init db and fos user create and fos user promote');

        if (null === $consoleDir)
        {
            return;
        }

        self::executeCommand($event,$consoleDir, 'doctrine:schema:update --force');

        /**
         * ROLE_USER need not be listed, see User::getRoles();
         */
        $users = [
            'admin' => ['ROLE_ADMIN'],
            'user1' => ['ROLE_NORMAL_USER','ROLE_CMS_USER'],
            'user2' => ['ROLE_CMS_USER'],
            'user3' => ['ROLE_NORMAL_USER'],
        ];
        foreach ($users as $username => $roles) {
            static::executeCommand($event,$consoleDir,"fos:user:create {$username} {$username}@straxus.com {$username}");
            foreach ($roles as $role) {
                static::executeCommand($event,$consoleDir, "fos:user:promote {$username} {$role}");
            }
        }
    }

}
