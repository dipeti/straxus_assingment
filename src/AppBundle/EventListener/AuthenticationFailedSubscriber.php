<?php
/**
 * Created by PhpStorm.
 * User: dipet
 * Date: 2017. 09. 30.
 * Time: 15:10
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;

class AuthenticationFailedSubscriber implements EventSubscriberInterface
{

    private $attributeBag;
    /**
     * AuthenticationFailedSubscriber constructor.
     */
    public function __construct(AttributeBagInterface $attributeBag)
    {
        $this->attributeBag = $attributeBag;
    }

    public function authenticationFailed(AuthenticationFailureEvent $event)
    {
        $number = $this->attributeBag->get(User::NUM_OF_FAILED_LOGINS_SESSION_KEY, 0);
        $this->attributeBag->set(User::NUM_OF_FAILED_LOGINS_SESSION_KEY, ++$number);
    }
    public static function getSubscribedEvents()
    {
        return ['security.authentication.failure' => 'authenticationFailed'];
    }

}