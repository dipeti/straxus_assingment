<?php
/**
 * Created by PhpStorm.
 * User: dipet
 * Date: 2017. 09. 30.
 * Time: 13:31
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RedirectUserSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;
    private $router;

    public function __construct(TokenStorageInterface $t, RouterInterface $r)
    {
        $this->tokenStorage = $t;
        $this->router = $r;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->isUserLogged() && $event->isMasterRequest()) {
            $currentRoute = $event->getRequest()->attributes->get('_route');
            if ($this->isAuthenticatedUserOnAnonymousPage($currentRoute)) {
                $response = new RedirectResponse($this->router->generate('homepage'));
                $event->setResponse($response);
            }
        }

    }

    private function isUserLogged()
    {
        if(!empty($this->tokenStorage->getToken()))
        {
            $user = $this->tokenStorage->getToken()->getUser();
            return $user instanceof User;
        }
        return false;

    }

    private function isAuthenticatedUserOnAnonymousPage($currentRoute)
    {
        return in_array(
            $currentRoute,
            ['fos_user_security_login', 'fos_user_resetting_request', 'fos_user_registration_register']
        );
    }

    public static function getSubscribedEvents()
    {
       return ['kernel.request' => 'onKernelRequest'];
    }
}