<?php
/**
 * Created by PhpStorm.
 * User: dipet
 * Date: 2017. 09. 30.
 * Time: 15:36
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthenticationSuccessSubscriber implements EventSubscriberInterface
{

    private $attributeBag;

    /**
     * AuthenticationFailedSubscriber constructor.
     */
    public function __construct(AttributeBagInterface $attributeBag)
    {
        $this->attributeBag = $attributeBag;
    }


    public function authenticationSuccess(InteractiveLoginEvent $event)
    {
        $this->attributeBag->remove(User::NUM_OF_FAILED_LOGINS_SESSION_KEY);
    }
    public static function getSubscribedEvents()
    {
        return ['security.interactive_login' => 'authenticationSuccess'];
    }

}