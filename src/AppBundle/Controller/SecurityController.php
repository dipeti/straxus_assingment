<?php

namespace AppBundle\Controller;

use Captcha\Bundle\CaptchaBundle\Security\Core\Exception\InvalidCaptchaException;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class SecurityController extends BaseController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $session = $request->getSession();
        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get captcha object instance
        $captcha = $this->get('captcha')->setConfig('LoginCaptcha');
        if ($request->isMethod('POST')) {
            if($request->request->has('captchaCode')){
                // validate the user-entered Captcha code when the form is submitted
                $code = $request->request->get('captchaCode');
                $isHuman = $captcha->Validate($code);
                if ($isHuman) {
                    // Captcha validation passed, check username and password
                    return $this->redirectToRoute('fos_user_security_check', [
                        'request' => $request], 307);
                } else {
                    // Captcha validation failed, set an invalid captcha exception in $authErrorKey attribute
                    $invalidCaptchaEx = new InvalidCaptchaException('CAPTCHA validation failed, try again.');
                    $request->attributes->set($authErrorKey, $invalidCaptchaEx);

                    // set last username entered by the user
                    $username = $request->request->get('_username', null);
                    $request->getSession()->set($lastUsernameKey, $username);
                }
            }
            else
            {
                return $this->redirectToRoute('fos_user_security_check', [
                    'request' => $request], 307);
            }
        }

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
            : null;

        return $this->renderLogin(array(
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
            'captcha_html' => $captcha->Html()
        ));
    }
}
