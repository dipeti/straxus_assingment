<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/admin", name="admin_page")
     * @param Request $request
     */
    public function showAdminPageAction(Request $request)
    {
        return $this->render(':default:admin.html.twig');
    }

    /**
     * @Route("/user/cms", name="cms_user_page")
     * @Security("has_role('ROLE_CMS_USER')")
     */
    public function showCMSUserPageAction()
    {
        return $this->render(':default:cms_user.html.twig');
    }

    /**
     * @Route("/user", name="normal_user_page")
     * @Security("has_role('ROLE_NORMAL_USER')")
     */
    public function showNormalUserPageAction()
    {
        return $this->render(':default:normal_user.html.twig');
    }
}
